//
//  firstswiftuiApp.swift
//  firstswiftui
//
//  Created by chen chicha on 2023/3/11.
//

import SwiftUI

@main
struct firstswiftuiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
