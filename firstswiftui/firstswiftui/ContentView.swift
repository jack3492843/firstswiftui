//
//  ContentView.swift
//  firstswiftui
//
//  Created by chen chicha on 2023/3/11.
//

import SwiftUI

struct ContentView: View {
    var tutors : [Tutor] = [];
    var body: some View {
        NavigationView{
            List(tutors) { tutor in
                TutorCell(tutor: tutor)
            }
            .navigationTitle(Text("Tutors"))
        }
        
    }
}
struct ContentView_Previews : PreviewProvider{
    static var previews : some View{
        ContentView(tutors: testData)
    }
}

struct TutorCell: View {
    let tutor:Tutor
    var body: some View {
        NavigationLink(destination: TutorDetail(name:tutor.name,headline: tutor.headline,bio:tutor.bio)) {
            HStack {
                Image(tutor.imageName)
                    .imageScale(.large)
                    .cornerRadius(60)
                VStack(alignment: .leading) {
                    Text(tutor.name)
                    Text(tutor.headline)
                        .font(.subheadline)
                        .foregroundColor(Color.gray)
                }
            }
        }
    }
}

